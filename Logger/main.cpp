﻿#include "Globals.h"

#include <iostream>
#include "LoggerListeners.h"
#include "Logger.h"

int main()
{
    cLogger::InitiateMultithreading();
    auto consoleLogListener = MakeConsoleListener(false);
    auto consoleAttachment = cLogger::GetInstance().AttachListener(std::move(consoleLogListener));
    cLogger::cAttachment fileAttachment;
    

    auto fileLogListenerRet = MakeFileListener();
    if (!fileLogListenerRet.first)
    {
        //m_TerminateEventRaised = true;
        LOGERROR("Failed to open log file, aborting");
        return 0;
    }
    fileAttachment = cLogger::GetInstance().AttachListener(std::move(fileLogListenerRet.second));


    LOGWARN("Regenerating settings.ini, all settings will be reset");
    
    LOGERROR("Failed to open log file, aborting");
    LOG("OK");
    std::cout << "Hello World!\n";
}
